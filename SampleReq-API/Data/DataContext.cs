using SampleReq_API.Models;
using Microsoft.EntityFrameworkCore;

namespace SampleReq_API.Data 
{
    public class DataContext : DbContext
    {
        public DataContext( DbContextOptions <DataContext> options) : base(options){}

        public DbSet<NewRequest> NewRequest {get; set;}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
// warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Server=fcsqldev\dev;Database=Core;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           modelBuilder.Entity<NewRequest>(entity =>
            {
                entity.Property(e => e.Approval)
                    .IsRequired()
                    .HasColumnName("approval")
                    .HasMaxLength(10);
                    
                
                entity.Property(e => e.Container)
                    .IsRequired()
                    .HasColumnName("container")
                    .HasMaxLength(100);

                entity.Property(e => e.CrtdDate).HasColumnName("crtd_date");

                entity.Property(e => e.CrtdUser)
                    .HasColumnName("crtd_user")
                    .HasMaxLength(50);

                entity.Property(e => e.Date).HasColumnName("date");

                entity.Property(e => e.LupdDate).HasColumnName("lupd_date");

                entity.Property(e => e.LupdUser)
                    .HasColumnName("lupd_user")
                    .HasMaxLength(50);
                
                entity.Property(e => e.Notes)
                    .IsRequired()
                    .HasColumnName("notes")
                    .HasMaxLength(255);

                entity.Property(e => e.Quantity)
                    .IsRequired()
                    .HasColumnName("quantity")
                    .HasMaxLength(10);

                entity.Property(e => e.Requestor)
                    .IsRequired()
                    .HasColumnName("requestor")
                    .HasMaxLength(50);

                entity.Property(e => e.Sample)
                    .IsRequired()
                    .HasColumnName("sample")
                    .HasMaxLength(255);

            });

          
        }
    }
}