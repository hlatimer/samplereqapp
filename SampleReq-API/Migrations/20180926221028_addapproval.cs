﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SampleReqAPI.Migrations
{
    public partial class addapproval : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "approval",
                table: "NewRequest",
                maxLength: 5,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "approval",
                table: "NewRequest");
        }
    }
}
