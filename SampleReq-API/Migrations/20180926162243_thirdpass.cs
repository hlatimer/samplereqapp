﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SampleReqAPI.Migrations
{
    public partial class thirdpass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "lupd_User",
                table: "NewRequest",
                newName: "lupd_user");

            migrationBuilder.RenameColumn(
                name: "crtd_User",
                table: "NewRequest",
                newName: "crtd_user");

            migrationBuilder.RenameColumn(
                name: "crtd_Date",
                table: "NewRequest",
                newName: "crtd_date");

            migrationBuilder.AlterColumn<string>(
                name: "sample",
                table: "NewRequest",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "char(20)",
                oldDefaultValueSql: "('')");

            migrationBuilder.AlterColumn<string>(
                name: "requestor",
                table: "NewRequest",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "char(8)",
                oldDefaultValueSql: "('')");

            migrationBuilder.AlterColumn<string>(
                name: "quantity",
                table: "NewRequest",
                maxLength: 10,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "char(8)",
                oldNullable: true,
                oldDefaultValueSql: "('')");

            migrationBuilder.AlterColumn<string>(
                name: "lupd_user",
                table: "NewRequest",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "char(10)",
                oldDefaultValueSql: "('')");

            migrationBuilder.AlterColumn<DateTime>(
                name: "lupd_date",
                table: "NewRequest",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "smalldatetime",
                oldDefaultValueSql: "(getdate())");

            migrationBuilder.AlterColumn<DateTime>(
                name: "date",
                table: "NewRequest",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "smalldatetime",
                oldDefaultValueSql: "(getdate())");

            migrationBuilder.AlterColumn<string>(
                name: "crtd_user",
                table: "NewRequest",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "char(10)",
                oldDefaultValueSql: "('')");

            migrationBuilder.AlterColumn<DateTime>(
                name: "crtd_date",
                table: "NewRequest",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "smalldatetime",
                oldDefaultValueSql: "(getdate())");

            migrationBuilder.AlterColumn<string>(
                name: "container",
                table: "NewRequest",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "char(10)",
                oldNullable: true,
                oldDefaultValueSql: "('')");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "NewRequest",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "char(10)")
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "lupd_user",
                table: "NewRequest",
                newName: "lupd_User");

            migrationBuilder.RenameColumn(
                name: "crtd_user",
                table: "NewRequest",
                newName: "crtd_User");

            migrationBuilder.RenameColumn(
                name: "crtd_date",
                table: "NewRequest",
                newName: "crtd_Date");

            migrationBuilder.AlterColumn<string>(
                name: "sample",
                table: "NewRequest",
                type: "char(20)",
                nullable: false,
                defaultValueSql: "('')",
                oldClrType: typeof(string),
                oldMaxLength: 255);

            migrationBuilder.AlterColumn<string>(
                name: "requestor",
                table: "NewRequest",
                type: "char(8)",
                nullable: false,
                defaultValueSql: "('')",
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "quantity",
                table: "NewRequest",
                type: "char(8)",
                nullable: true,
                defaultValueSql: "('')",
                oldClrType: typeof(string),
                oldMaxLength: 10);

            migrationBuilder.AlterColumn<string>(
                name: "lupd_User",
                table: "NewRequest",
                type: "char(10)",
                nullable: false,
                defaultValueSql: "('')",
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "lupd_date",
                table: "NewRequest",
                type: "smalldatetime",
                nullable: false,
                defaultValueSql: "(getdate())",
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<DateTime>(
                name: "date",
                table: "NewRequest",
                type: "smalldatetime",
                nullable: false,
                defaultValueSql: "(getdate())",
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<string>(
                name: "crtd_User",
                table: "NewRequest",
                type: "char(10)",
                nullable: false,
                defaultValueSql: "('')",
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "crtd_Date",
                table: "NewRequest",
                type: "smalldatetime",
                nullable: false,
                defaultValueSql: "(getdate())",
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<string>(
                name: "container",
                table: "NewRequest",
                type: "char(10)",
                nullable: true,
                defaultValueSql: "('')",
                oldClrType: typeof(string),
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<string>(
                name: "Id",
                table: "NewRequest",
                type: "char(10)",
                nullable: false,
                oldClrType: typeof(int))
                .OldAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);
        }
    }
}
