﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SampleReqAPI.Migrations
{
    public partial class addednotes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "notes",
                table: "NewRequest",
                maxLength: 255,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "notes",
                table: "NewRequest");
        }
    }
}
