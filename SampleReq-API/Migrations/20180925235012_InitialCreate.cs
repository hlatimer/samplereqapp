﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SampleReqAPI.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NewRequest",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    requestor = table.Column<string>(nullable: true),
                    date = table.Column<DateTime>(nullable: false),
                    sample = table.Column<string>(nullable: true),
                    container = table.Column<string>(nullable: true),
                    quantity = table.Column<string>(nullable: true),
                    crtd_user = table.Column<string>(nullable: true),
                    crtd_date = table.Column<DateTime>(nullable: false),
                    lupd_user = table.Column<string>(nullable: true),
                    lupd_date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewRequest", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NewRequest");
        }
    }
}
