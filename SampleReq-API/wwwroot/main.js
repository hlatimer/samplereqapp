(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/ProdList/ProdList.component.css":
/*!*************************************************!*\
  !*** ./src/app/ProdList/ProdList.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/ProdList/ProdList.component.html":
/*!**************************************************!*\
  !*** ./src/app/ProdList/ProdList.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">\n\n<label for=\"inputCity\">New Product</label>\n<div class=\"col-auto my-2\">\n       \n    <div class='btn-group'>\n            \n        <button type='button' class=\"btn btn-dark\" (click)=\" this.openEdits =! this.openEdits\"\n        *ngIf='this.openEdits == false'\n        (click)=\"newprod.productname=' '\">Add Product</button>\n        \n                \n        <select  class=\"custom-select col-2-offset-4\" \n        id=\"inputGroupSelect03\" [(ngModel)]=\"newprod.productname\"\n        name=\"newproduct\"\n        #newproduct\n        *ngIf='this.openEdits == true'>\n            <option *ngFor='let value of prodnames;' [value]='value.Text'\n            >{{value.Text}}</option>\n        </select>\n\n        <button type=\"button\" class=\"btn btn-primary\"\n        *ngIf='this.openEdits == true'\n        (click)='save(newprod)'\n        (click)=\"newprod.productname=' '\" \n        [disabled]=\"newprod.productname == ' '\" >Save</button>\n\n        <button type=\"button\" \n        class=\"btn btn-danger\"\n        *ngIf='this.openEdits == true'\n        (click)='this.openEdits =! this.openEdits'>Cancel</button>\n\n                    \n    </div>\n</div>\n\n\n\n    "

/***/ }),

/***/ "./src/app/ProdList/ProdList.component.ts":
/*!************************************************!*\
  !*** ./src/app/ProdList/ProdList.component.ts ***!
  \************************************************/
/*! exports provided: ProdListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProdListComponent", function() { return ProdListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_request_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../_services/request.service */ "./src/app/_services/request.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../globals */ "./src/app/globals.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProdListComponent = /** @class */ (function () {
    function ProdListComponent(reqservice, router, toastr, globals) {
        this.reqservice = reqservice;
        this.router = router;
        this.toastr = toastr;
        this.globals = globals;
        this.openEdits = false;
        this.prodnames = [];
        this.newprod = { productname: '', user: '' };
    }
    // Populate new product control input with products that are NOT
    // already present in the product request list.
    ProdListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.reqservice.getprods().subscribe(function (res) {
            _this.prodnames = res;
        });
        this.activeUser();
    };
    // Save new product to product list via API
    ProdListComponent.prototype.save = function () {
        var _this = this;
        this.newprod.user = this.activeuser.userId;
        this.reqservice.addprod(this.newprod).subscribe(function () {
            _this.toastr.success('New Product Added', 'Submission Success');
            _this.router.navigate(['/reqgrid']);
        }, function (error) {
            console.log(error);
        });
    };
    ProdListComponent.prototype.activeUser = function () {
        var _this = this;
        this.reqservice.getUser().subscribe(function (user) {
            _this.activeuser = user;
            _this.globals.userId = _this.activeuser.userId;
            _this.globals.securityGroup = _this.activeuser.securityGroup;
            _this.secGroup = _this.globals.securityGroup;
        });
    };
    ProdListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-prodlist, [app-prodlist]',
            template: __webpack_require__(/*! ./ProdList.component.html */ "./src/app/ProdList/ProdList.component.html"),
            styles: [__webpack_require__(/*! ./ProdList.component.css */ "./src/app/ProdList/ProdList.component.css")]
        }),
        __metadata("design:paramtypes", [_services_request_service__WEBPACK_IMPORTED_MODULE_1__["RequestService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"], _globals__WEBPACK_IMPORTED_MODULE_4__["Globals"]])
    ], ProdListComponent);
    return ProdListComponent;
}());



/***/ }),

/***/ "./src/app/ReqForm/ReqForm.component.css":
/*!***********************************************!*\
  !*** ./src/app/ReqForm/ReqForm.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "button{\r\n    margin: 8px;\r\n  }"

/***/ }),

/***/ "./src/app/ReqForm/ReqForm.component.html":
/*!************************************************!*\
  !*** ./src/app/ReqForm/ReqForm.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">\n\n<html lang='en'>\n    \n  <div *ngIf=\"this.secGroup == ''\"></div>\n    \n  <div class=\"card bg-transparent border-0\" style=\"width: 70rem;margin:0 auto;\" *ngIf=\"this.secGroup == 'Admin' || 'Edit' || 'Power'\"> \n    <form style=\"border:1px solid black\" [formGroup]='requestForm' (ngSubmit)='submit()' >\n        <div class='form-row'>  \n\n          <div>\n            <button type=\"button\" \n            class=\"btn btn-success\" \n            (click)='this.newRequest =! this.newRequest'\n            (click)='this.requestEdits.emit(this.newRequest);' \n            style=\"float: left\" \n            [disabled]='disabled == true || this.newRequest == true'>\n            New Request</button>\n\n            <button type=\"button\" \n            class=\"btn btn-danger\"\n            *ngIf='this.newRequest'\n            (click)='this.newRequest =! this.newRequest'\n            (click)='this.requestEdits.emit(this.newRequest);'\n            >Cancel</button>\n          </div>\n        </div>\n\n      <div *ngIf='this.newRequest'> \n\n        <div class=\"form-row\">\n          <div class=\"form-group col-md-2 \"></div>\n          <div class=\"form-group col-md-4 \" style=\"float: center\">\n          <label for=\"inputPassword4\">Request Date</label>\n          <input type=\"date\"  class=\"form-control\" \n            formControlName=\"date\" value=\"{{ this.curDate | date:'yyyy-MM-dd'}}\">\n        </div>\n          \n        <div class=\"form-group col-md-4\">\n          <label for=\"inputCity\">Container</label>\n            <select formControlName=\"container\" class=\"form-control\" required >\n              <option>QC Bottle</option>\n              <option>10kg Carboy</option>\n              <option>20kg Carboy</option>\n              <option>Drum</option>\n              <option>TOTE</option>\n            </select>\n          </div>\n\n        </div>\n          \n        \n        <div class=\"form-row align-items-center\">\n            <div class=\"form-group col-md-2\" ></div>\n            <div class=\"col-sm-3 my-1\">\n                \n                <label for=\"inputCity\">Product</label>\n              <select type=\"text\" \n              class=\"form-control\" \n              id=\"inlineFormInputName\" \n              formControlName=\"sample\"\n              required\n              placeholder=\"Product\">\n              <option></option>\n              <option *ngFor='let value of prodnamereq ; let i = index' \n              >{{value.Text}}</option>\n              </select>\n            </div>\n\n\n            <div class=\"col-sm-1 my-1\">\n              <label for=\"inputCity\">Qty</label>\n              <div class=\"input-group\">\n                  <input type=\"text\" class=\"form-control\" id=\"inputZip\"\n                  formControlName=\"quantity\">\n              </div>\n            </div>\n\n            <app-prodlist></app-prodlist>\n          \n          </div>\n\n        <div class='form-row'>\n          <div class=\"form-group col-md-2 \"></div>\n          <div class=\"form-group col-md-8\" >\n              <label>Notes</label>\n              <input type=\"text\" class=\"form-control\" formControlName = 'notes'>\n            </div>\n          </div>\n\n        <div class='btn-group'>\n            <button type=\"submit\" class=\"btn btn-primary\" [disabled]=\"!requestForm.valid\">Submit</button>\n            <button type=\"button\" class=\"btn btn-danger\" (click)='resetForm()' >Clear Form</button>\n        </div>\n\n        \n      </div>\n    </form>\n    <br>\n  </div>\n</html>\n\n"

/***/ }),

/***/ "./src/app/ReqForm/ReqForm.component.ts":
/*!**********************************************!*\
  !*** ./src/app/ReqForm/ReqForm.component.ts ***!
  \**********************************************/
/*! exports provided: ReqFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReqFormComponent", function() { return ReqFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_request_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_services/request.service */ "./src/app/_services/request.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ReqFormComponent = /** @class */ (function () {
    function ReqFormComponent(fb, reqservice, route, router, toastr) {
        this.fb = fb;
        this.reqservice = reqservice;
        this.route = route;
        this.router = router;
        this.toastr = toastr;
        this.prodnamereq = [];
        this.curDate = new Date();
        this.newRequest = false;
        this.requestEdits = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    Object.defineProperty(ReqFormComponent.prototype, "disableReqs", {
        set: function (message) {
            this.disabled = message;
        },
        enumerable: true,
        configurable: true
    });
    ReqFormComponent.prototype.ngOnInit = function () {
        this.activeUser();
        this.loadprods();
        this.createRequestForm();
        this.curDate = new Date();
    };
    // Creates From group and adds default values to form controls
    ReqFormComponent.prototype.createRequestForm = function () {
        this.requestForm = this.fb.group({
            requestor: [''],
            date: [this.curDate, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            sample: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            container: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            quantity: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            crtdUser: [''],
            crtdDate: [this.curDate],
            approval: ['Pending'],
            notes: [''],
        });
    };
    // Sends new request to API to save request and reloads page to update values.
    // Recreates form and resets form controls
    ReqFormComponent.prototype.submit = function () {
        var _this = this;
        this.requestForm.controls['requestor'].setValue(this.activeuser.userId);
        this.requestForm.controls['crtdUser'].setValue(this.activeuser.userId);
        this.request = Object.assign({}, this.requestForm.value);
        this.reqservice.submitrequest(this.request).subscribe(function () {
            _this.toastr.success('Request Submitted', 'Submission Success');
            _this.router.navigate(['/reqgrid']);
        }, function (error) {
            console.log(error);
        });
        this.createRequestForm();
        this.requestForm.controls['requestor'].setValue(this.activeuser.userId);
        this.curDate = new Date();
        console.log(this.curDate);
        this.requestForm.controls['date'].setValue(this.curDate);
    };
    // Populates product list from API using resolver
    ReqFormComponent.prototype.loadprods = function () {
        var _this = this;
        this.route.data.subscribe(function (data) {
            _this.prodnamereq = data['reqform'];
            _this.activeUser();
        });
    };
    // Gets current user from API
    ReqFormComponent.prototype.activeUser = function () {
        var _this = this;
        this.reqservice.getUser().subscribe(function (user) {
            _this.activeuser = user;
            _this.secGroup = user.securityGroup;
        });
    };
    ReqFormComponent.prototype.resetForm = function () {
        this.createRequestForm();
        this.requestForm.controls['requestor'].setValue(this.activeuser.userId);
        this.curDate = new Date();
        console.log(this.curDate);
        this.requestForm.controls['date'].setValue(this.curDate);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], ReqFormComponent.prototype, "requestEdits", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Boolean])
    ], ReqFormComponent.prototype, "disableReqs", null);
    ReqFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reqform',
            template: __webpack_require__(/*! ./ReqForm.component.html */ "./src/app/ReqForm/ReqForm.component.html"),
            styles: [__webpack_require__(/*! ./ReqForm.component.css */ "./src/app/ReqForm/ReqForm.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _services_request_service__WEBPACK_IMPORTED_MODULE_2__["RequestService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], ReqFormComponent);
    return ReqFormComponent;
}());



/***/ }),

/***/ "./src/app/ReqGrid/ReqGrid.component.css":
/*!***********************************************!*\
  !*** ./src/app/ReqGrid/ReqGrid.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.btn-success {\r\n    box-shadow: none;\r\n    margin: 5px;\r\n}\r\n\r\n.btn-danger{\r\n    margin: 5px;\r\n}"

/***/ }),

/***/ "./src/app/ReqGrid/ReqGrid.component.html":
/*!************************************************!*\
  !*** ./src/app/ReqGrid/ReqGrid.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "User:{{activeuser?.userId}}\n<br>              \nSecurity Group:{{secGroup}}\n<app-reqform (requestEdits)='validEdits($event)' [(disableReqs)]='disable'></app-reqform>\n<br>\n\n<div *ngIf=\"this.secGroup == ''\" >\n  <h1>Access Denied to {{activeuser?.userId}}</h1>\n</div>\n\n<form *ngIf=\"this.secGroup == 'Admin' || 'Edit' || 'Power'\">\n  <div class='table-responsive' style=\"width: 70rem;margin:0 auto;\">\n      <button type=\"button\" class=\"btn btn-success\" style=\"float: left\" \n      (click)='this.editRequests =! this.editRequests'\n      (click)='touched = false'\n      (click)='this.disable = true'\n      [disabled]='valid != false || editRequests == true'>\n      Edit\n     </button>\n  <form style=\"border:1px solid black\" #requestForm=\"ngForm\" (ngSubmit)='saveedits()'>\n      <button class=\"btn btn-success\" \n       type=\"submit\" \n       *ngIf='editRequests' \n       [disabled]=\"!touched\" \n       style=\"float: left\"\n        >Save</button>\n      <button type=\"button\" \n      *ngIf='editRequests'\n      class=\"btn btn-danger\"\n      style=\"float: left\"\n      (click)='this.disable = false'\n      (click)='this.editRequests =! this.editRequests'>Cancel</button>\n    <table class='table table-bordered'>\n      <thead>\n        <tr>\n          <th class='text-center'>Request ID</th>\n          <th class='text-center'>Requestor</th>\n          <th class='text-center'>Request Date</th>\n          <th class='text-center'>Product</th>\n          <th class='text-center'>Container</th>\n          <th class='text-center'>Qty</th>\n          <th class='text-center'>Notes</th>\n          <th class='text-center'>Status</th>\n        </tr>\n      </thead>\n      <tbody *ngIf='!editRequests'>\n        <tr *ngFor='let request of requests; let i = index;'>\n          <ng-container *ngIf=\"requests[i].approval == 'Approved' || requests[i].approval == 'Pending' \" >\n          <td>{{request.id}}</td>\n          <td>{{request.requestor}}</td>\n          <td>{{request.date | date}}</td>\n          <td>{{request.sample}}</td>\n          <td>{{request.container}}</td>\n          <td>{{request.quantity}}</td>\n          <td>{{request.notes}}</td>\n          <td>{{request.approval}}</td>\n        </ng-container>\n        </tr>\n      </tbody>\n      <tbody *ngIf='editRequests'>\n        <tr *ngFor='let request of requests; let i = index;'>\n          <ng-container *ngIf=\"requests[i].approval == 'Approved' || requests[i].approval == 'Pending' \" >\n            <td>{{request.id}}</td>\n            <td>{{request.requestor}}</td>\n            <ng-container *ngIf=\"this.activeuser.userId != request.requestor && this.secGroup != 'Power'\">\n                <td>{{request.date | date}}</td>\n                <td>{{request.sample}}</td>\n                <td>{{request.container}}</td>\n                <td>{{request.quantity}}</td>\n                <td>{{request.notes}}</td>\n            </ng-container>\n            <ng-container *ngIf=\"this.activeuser.userId == request.requestor || this.secGroup == 'Power' \">\n              <td>\n                  <input  type=\"date\" \n                  name=\"date{{i}}\" \n                  [ngModel]=\"requests[i].date | date:'yyyy-MM-dd'\" \n                  (ngModelChange)=\"requests[i].date=$event\"\n                  (click)=\"touched = true\" \n                  (click)=\"recordsToEdit(i)\">\n              </td>\n              <td>\n                <select [value]='requests.sample' size=\"1\"  [(ngModel)]='requests[i].sample' \n                name='sample{{i}}' (click)=\"touched = true\">\n                  <option  selected *ngFor='let value of prodnamereq;' (click)=\"recordsToEdit(i)\"\n                  >{{value.Text}}</option>\n                </select>\n              </td>\n              <td>\n                  <select [value]='requests.container' size=\"1\"  [(ngModel)]='requests[i].container' \n                  name='container{{i}}'  (click)=\"touched = true\" >\n                    <option  selected *ngFor='let value of containers;let in = index' (click)=\"recordsToEdit(i)\"\n                    >{{containers[in]}}</option>\n                  </select>\n              </td>\n              <td>\n                <input [value]='request.quantity' size=\"1\"  [(ngModel)]='requests[i].quantity' name='quantity{{i}}'\n                (click)=\"recordsToEdit(i)\" (click)=\"touched = true\">\n              </td>\n              \n              <td >\n                <input [value]='requests.notes' [(ngModel)]='requests[i].notes' name='notes{{i}}' (click)=\"touched = true\">\n              </td>\n            </ng-container>\n          \n            <td *ngIf=\"this.secGroup == 'Power' \">\n                <select  [value]='request.approval'  [(ngModel)]='requests[i].approval' name='approval{{i}}' \n                [ngModelOptions]=\"{updateOn: 'submit'}\" (click)=\"touched = true\" (click)=\"recordsToEdit(i)\">\n                <option selected>Pending</option>\n                <option>Approved</option>\n                <option>Finish</option>\n                </select>\n              </td>\n\n            <td *ngIf=\"this.secGroup == 'Admin' || 'Edit'\">{{request.approval}}</td>\n          </ng-container>\n      </tr>\n    </tbody>\n  </table>\n    \n  </form>\n  </div>\n</form>\n<br>\n\n\n\n"

/***/ }),

/***/ "./src/app/ReqGrid/ReqGrid.component.ts":
/*!**********************************************!*\
  !*** ./src/app/ReqGrid/ReqGrid.component.ts ***!
  \**********************************************/
/*! exports provided: ReqGridComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReqGridComponent", function() { return ReqGridComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_request_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../_services/request.service */ "./src/app/_services/request.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../globals */ "./src/app/globals.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ReqGridComponent = /** @class */ (function () {
    function ReqGridComponent(reqservice, route, router, toastr, globals) {
        var _this = this;
        this.reqservice = reqservice;
        this.route = route;
        this.router = router;
        this.toastr = toastr;
        this.globals = globals;
        this.containers = ['QC Bottle', '10kg Carboy', '20kg Carboy', 'Drum', 'TOTE'];
        this.editRequests = false;
        this.touched = false;
        this.statusChange = false;
        this.prodnamereq = [];
        this.editedIndices = [];
        this.requestEdits = [];
        this.navigationSubscription = this.router.events.subscribe(function (e) {
            // If it is a NavigationEnd event re-initalise the component
            if (e instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]) {
                _this.loadrequests();
            }
        });
    }
    ReqGridComponent.prototype.ngOnInit = function () {
        this.loadrequests();
        this.loadprods();
        this.activeUser();
        this.valid = false;
        this.disable = false;
    };
    ReqGridComponent.prototype.ngOnDestroy = function () {
        // avoid memory leaks here by cleaning up after ourselves. If we
        // don't then we will continue to run our initialiseInvites()
        // method on every navigationEnd event.
        if (this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    };
    // Collects the indices of rows that have been edited
    ReqGridComponent.prototype.recordsToEdit = function (val) {
        if (this.editedIndices.length === 0) {
            this.editedIndices.push(val);
        }
        else {
            if (this.contains(val, this.editedIndices) === false) {
                this.editedIndices.push(val);
            }
        }
    };
    // Helper function to see if index has been added to list of edited indices
    ReqGridComponent.prototype.contains = function (index, value) {
        for (var num = 0; num < value.length; num++) {
            if (index === value[num]) {
                return true;
            }
        }
        return false;
    };
    // Push records that have been edited to an array to be updated by API
    ReqGridComponent.prototype.requestPush = function () {
        for (var num = 0; num < this.editedIndices.length; num++) {
            this.requests[this.editedIndices[num]].lupdDate = new Date();
            // this.requests[this.editedIndices[num]].lupdUser = this.activeuser;
            this.requestEdits[num] = this.requests[this.editedIndices[num]];
        }
    };
    // Push edit records to API and alert users of changes to records
    ReqGridComponent.prototype.saveedits = function () {
        var _this = this;
        this.requestPush();
        this.reqservice.editrequests(this.requestEdits).subscribe(function () {
            if (_this.statusChange = true) {
                for (var num = 0; num < _this.requestEdits.length; num++) {
                    _this.reqservice.editalerts(_this.requestEdits[num]).subscribe(function () {
                    });
                }
            }
            _this.reset();
            _this.editRequests = !_this.editRequests;
            _this.disable = false;
        }, function (error) {
            console.log(error);
        });
    };
    // Resets arrays holding record indices and records and navigates to
    // ReqGrid compoonent to reload data on the page
    ReqGridComponent.prototype.reset = function () {
        this.requestEdits = [];
        this.editedIndices = [];
        this.router.navigate(['/reqgrid']);
        this.toastr.success('Edits Submitted', 'Submission Success');
        this.touched = false;
    };
    // Load requests from resolver to display and edit in componenet
    ReqGridComponent.prototype.loadrequests = function () {
        var _this = this;
        this.route.data.subscribe(function (data) {
            _this.requests = data['reqgrid'];
            _this.requests.sort(function (a, b) { return new Date(a.date).getTime() - new Date(b.date).getTime(); });
            _this.activeUser();
        }, function (error) {
            console.log(error);
        });
    };
    // Populate product list for edits from route resolver
    ReqGridComponent.prototype.loadprods = function () {
        var _this = this;
        this.route.data.subscribe(function (data) {
            _this.prodnamereq = data['reqform'];
        });
    };
    // Get current user and security group
    ReqGridComponent.prototype.activeUser = function () {
        var _this = this;
        this.reqservice.getUser().subscribe(function (user) {
            _this.activeuser = user;
            _this.globals.userId = _this.activeuser.userId;
            _this.globals.securityGroup = _this.activeuser.securityGroup;
            _this.secGroup = _this.globals.securityGroup;
        });
    };
    ReqGridComponent.prototype.validEdits = function (message) {
        this.valid = message;
    };
    ReqGridComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reqgrid',
            template: __webpack_require__(/*! ./ReqGrid.component.html */ "./src/app/ReqGrid/ReqGrid.component.html"),
            styles: [__webpack_require__(/*! ./ReqGrid.component.css */ "./src/app/ReqGrid/ReqGrid.component.css")]
        }),
        __metadata("design:paramtypes", [_services_request_service__WEBPACK_IMPORTED_MODULE_1__["RequestService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"], _globals__WEBPACK_IMPORTED_MODULE_4__["Globals"]])
    ], ReqGridComponent);
    return ReqGridComponent;
}());



/***/ }),

/***/ "./src/app/_intercepts/win.auth.interceptor.ts":
/*!*****************************************************!*\
  !*** ./src/app/_intercepts/win.auth.interceptor.ts ***!
  \*****************************************************/
/*! exports provided: WinAuthInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WinAuthInterceptor", function() { return WinAuthInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WinAuthInterceptor = /** @class */ (function () {
    function WinAuthInterceptor() {
    }
    WinAuthInterceptor.prototype.intercept = function (request, next) {
        request = request.clone({
            withCredentials: true
        });
        return next.handle(request);
    };
    WinAuthInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], WinAuthInterceptor);
    return WinAuthInterceptor;
}());



/***/ }),

/***/ "./src/app/_resolver/product.resolver.ts":
/*!***********************************************!*\
  !*** ./src/app/_resolver/product.resolver.ts ***!
  \***********************************************/
/*! exports provided: ProductResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductResolver", function() { return ProductResolver; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_request_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_services/request.service */ "./src/app/_services/request.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductResolver = /** @class */ (function () {
    function ProductResolver(router, reqservice) {
        this.router = router;
        this.reqservice = reqservice;
    }
    ProductResolver.prototype.resolve = function (route, state) {
        return this.reqservice.getprodsreqs();
    };
    ProductResolver = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({ providedIn: 'root' }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_request_service__WEBPACK_IMPORTED_MODULE_2__["RequestService"]])
    ], ProductResolver);
    return ProductResolver;
}());



/***/ }),

/***/ "./src/app/_resolver/request.resolver.ts":
/*!***********************************************!*\
  !*** ./src/app/_resolver/request.resolver.ts ***!
  \***********************************************/
/*! exports provided: RequestResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestResolver", function() { return RequestResolver; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_request_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_services/request.service */ "./src/app/_services/request.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RequestResolver = /** @class */ (function () {
    function RequestResolver(router, reqservice) {
        this.router = router;
        this.reqservice = reqservice;
    }
    RequestResolver.prototype.resolve = function (route, state) {
        return this.reqservice.getrequests();
    };
    RequestResolver = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({ providedIn: 'root' }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_request_service__WEBPACK_IMPORTED_MODULE_2__["RequestService"]])
    ], RequestResolver);
    return RequestResolver;
}());



/***/ }),

/***/ "./src/app/_services/request.service.ts":
/*!**********************************************!*\
  !*** ./src/app/_services/request.service.ts ***!
  \**********************************************/
/*! exports provided: RequestService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestService", function() { return RequestService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/internal/operators/map */ "./node_modules/rxjs/internal/operators/map.js");
/* harmony import */ var rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RequestService = /** @class */ (function () {
    function RequestService(http) {
        this.http = http;
        this.headerDict = {
            'Content-Type': 'application/json',
        };
    }
    RequestService.prototype.getrequests = function () {
        return this.http.get('http://localhost:5000/api/requests');
    };
    RequestService.prototype.getprods = function () {
        return this.http.get('http://localhost:5000/api/requests/prodnames');
    };
    RequestService.prototype.getprodsreqs = function () {
        return this.http.get('http://localhost:5000/api/requests/prodnamesreq');
    };
    RequestService.prototype.addprod = function (newProd) {
        return this.http.post('http://localhost:5000/api/requests/newprod', newProd, { headers: this.headerDict });
    };
    RequestService.prototype.submitrequest = function (newRequest) {
        return this.http.post('http://localhost:5000/api/requests', newRequest, { headers: this.headerDict });
    };
    RequestService.prototype.editrequests = function (edits) {
        return this.http.post('http://localhost:5000/api/requests/edits', edits, { headers: this.headerDict });
    };
    RequestService.prototype.editalerts = function (edits) {
        return this.http.post('http://localhost:5000/api/requests/editsalert', edits, { headers: this.headerDict });
    };
    RequestService.prototype.getUser = function () {
        var _this = this;
        return this.http.get('http://localhost:5000/api/user', { observe: 'response' })
            .pipe(Object(rxjs_internal_operators_map__WEBPACK_IMPORTED_MODULE_2__["map"])(function (response) {
            var user = response;
            if (user) {
                _this.currentUser = user.body;
            }
            return _this.currentUser;
        }));
    };
    RequestService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], RequestService);
    return RequestService;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<div style=\"text-align:center\">\n  <app-user-detail></app-user-detail>\n  <router-outlet></router-outlet>\n</div>\n  \n  \n\n  \n  \n  \n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'SampleReqSPA';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./globals */ "./src/app/globals.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _ReqForm_ReqForm_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ReqForm/ReqForm.component */ "./src/app/ReqForm/ReqForm.component.ts");
/* harmony import */ var _ReqGrid_ReqGrid_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ReqGrid/ReqGrid.component */ "./src/app/ReqGrid/ReqGrid.component.ts");
/* harmony import */ var ng2_datepicker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-datepicker */ "./node_modules/ng2-datepicker/dist/bundles/ng2-datepicker.umd.js");
/* harmony import */ var ng2_datepicker__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_datepicker__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_request_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./_services/request.service */ "./src/app/_services/request.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _routes__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./routes */ "./src/app/routes.ts");
/* harmony import */ var _resolver_request_resolver__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./_resolver/request.resolver */ "./src/app/_resolver/request.resolver.ts");
/* harmony import */ var _ProdList_ProdList_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./ProdList/ProdList.component */ "./src/app/ProdList/ProdList.component.ts");
/* harmony import */ var _resolver_product_resolver__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./_resolver/product.resolver */ "./src/app/_resolver/product.resolver.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _intercepts_win_auth_interceptor__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./_intercepts/win.auth.interceptor */ "./src/app/_intercepts/win.auth.interceptor.ts");
/* harmony import */ var _user_detail_user_detail_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./user-detail/user-detail.component */ "./src/app/user-detail/user-detail.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _ReqGrid_ReqGrid_component__WEBPACK_IMPORTED_MODULE_5__["ReqGridComponent"],
                _ReqForm_ReqForm_component__WEBPACK_IMPORTED_MODULE_4__["ReqFormComponent"],
                _ProdList_ProdList_component__WEBPACK_IMPORTED_MODULE_14__["ProdListComponent"],
                _user_detail_user_detail_component__WEBPACK_IMPORTED_MODULE_19__["UserDetailComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                ng2_datepicker__WEBPACK_IMPORTED_MODULE_6__["NgDatepickerModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_10__["HttpModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_11__["RouterModule"].forRoot(_routes__WEBPACK_IMPORTED_MODULE_12__["appRoutes"], { onSameUrlNavigation: 'reload' }),
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_16__["BrowserAnimationsModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_17__["ToastrModule"].forRoot()
            ],
            providers: [
                _services_request_service__WEBPACK_IMPORTED_MODULE_8__["RequestService"],
                _resolver_request_resolver__WEBPACK_IMPORTED_MODULE_13__["RequestResolver"],
                _resolver_product_resolver__WEBPACK_IMPORTED_MODULE_15__["ProductResolver"],
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HTTP_INTERCEPTORS"],
                    useClass: _intercepts_win_auth_interceptor__WEBPACK_IMPORTED_MODULE_18__["WinAuthInterceptor"],
                    multi: true
                },
                _globals__WEBPACK_IMPORTED_MODULE_2__["Globals"]
            ],
            bootstrap: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]
            ]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/globals.ts":
/*!****************************!*\
  !*** ./src/app/globals.ts ***!
  \****************************/
/*! exports provided: Globals */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Globals", function() { return Globals; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var Globals = /** @class */ (function () {
    function Globals() {
    }
    Globals = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], Globals);
    return Globals;
}());



/***/ }),

/***/ "./src/app/routes.ts":
/*!***************************!*\
  !*** ./src/app/routes.ts ***!
  \***************************/
/*! exports provided: appRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "appRoutes", function() { return appRoutes; });
/* harmony import */ var _ReqGrid_ReqGrid_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ReqGrid/ReqGrid.component */ "./src/app/ReqGrid/ReqGrid.component.ts");
/* harmony import */ var _resolver_request_resolver__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./_resolver/request.resolver */ "./src/app/_resolver/request.resolver.ts");
/* harmony import */ var _resolver_product_resolver__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./_resolver/product.resolver */ "./src/app/_resolver/product.resolver.ts");



var appRoutes = [
    { path: 'reqgrid',
        component: _ReqGrid_ReqGrid_component__WEBPACK_IMPORTED_MODULE_0__["ReqGridComponent"],
        resolve: { reqgrid: _resolver_request_resolver__WEBPACK_IMPORTED_MODULE_1__["RequestResolver"], reqform: _resolver_product_resolver__WEBPACK_IMPORTED_MODULE_2__["ProductResolver"] },
        runGuardsAndResolvers: 'always' },
    { path: '', redirectTo: 'reqgrid', pathMatch: 'full', runGuardsAndResolvers: 'always' }
];


/***/ }),

/***/ "./src/app/user-detail/user-detail.component.css":
/*!*******************************************************!*\
  !*** ./src/app/user-detail/user-detail.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user-detail/user-detail.component.html":
/*!********************************************************!*\
  !*** ./src/app/user-detail/user-detail.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user-detail/user-detail.component.ts":
/*!******************************************************!*\
  !*** ./src/app/user-detail/user-detail.component.ts ***!
  \******************************************************/
/*! exports provided: UserDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserDetailComponent", function() { return UserDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../globals */ "./src/app/globals.ts");
/* harmony import */ var _services_request_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_services/request.service */ "./src/app/_services/request.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserDetailComponent = /** @class */ (function () {
    function UserDetailComponent(reqservice, globals) {
        this.reqservice = reqservice;
        this.globals = globals;
        this.initialized = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.notauthorized = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    UserDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.reqservice.getUser().subscribe(function (user) {
            _this.user = user;
            console.log('plz work');
        }, function (error) {
            console.log(error);
        }, function () {
            _this.globals.userId = _this.user.userId;
            _this.globals.securityGroup = _this.user.securityGroup;
            _this.secGroup = _this.globals.securityGroup;
            console.log('please work');
            console.log(_this.user.userId);
            console.log(_this.user.securityGroup);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], UserDetailComponent.prototype, "initialized", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], UserDetailComponent.prototype, "notauthorized", void 0);
    UserDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-detail',
            template: __webpack_require__(/*! ./user-detail.component.html */ "./src/app/user-detail/user-detail.component.html"),
            styles: [__webpack_require__(/*! ./user-detail.component.css */ "./src/app/user-detail/user-detail.component.css")]
        }),
        __metadata("design:paramtypes", [_services_request_service__WEBPACK_IMPORTED_MODULE_2__["RequestService"], _globals__WEBPACK_IMPORTED_MODULE_1__["Globals"]])
    ], UserDetailComponent);
    return UserDetailComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\hlatimer.FC\SampleReqApp\SampleReqSPA\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map