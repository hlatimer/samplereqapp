using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SampleReq_API.Models
{
    public class NewRequest {

    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
     public int Id { get; set; }
        public string Requestor { get; set; }
        public DateTime Date { get; set; }
        public string Sample { get; set; }
        public string Container { get; set; }
        public string Quantity { get; set; }
        public string CrtdUser { get; set; }
        public DateTime CrtdDate { get; set; }
        public string LupdUser { get; set; }
        public DateTime LupdDate { get; set; }
        public string Approval { get; set; }
        public string Notes {get; set;}
    }
}