﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

//using AutoMapper;
using SampleReq_API.Data;

namespace SampleReq_API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            var connection = @"Server=FC-SQL-V\DEV;Database=FSky_Apps;User Id=FSky_User;Password=D@tAWr1t3r*;";
            services.AddDbContext<DataContext>(options => options.UseSqlServer(connection));
            services.AddCors();

            //services.AddAutoMapper();
            services.AddAuthorization(options =>
            {
                var adminGroup = Configuration.GetValue<string>("WindowsAdminGroup");
                var powerGroup = Configuration.GetValue<string>("WindowsPowerGroup");
                var editGroup = Configuration.GetValue<string>("WindowsEditGroup");
                var readGroup = Configuration.GetValue<string>("WindowsReadGroup");

                options.AddPolicy("RequireAdminRole", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    if (adminGroup != null)
                        policy.RequireRole(adminGroup);
                });

                options.AddPolicy("RequirePowerRole", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    if (powerGroup != null)
                        policy.RequireRole(powerGroup);
                });

                options.AddPolicy("RequireEditRole", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    if (editGroup != null)
                        policy.RequireRole(editGroup, adminGroup);
                });

                options.AddPolicy("RequireReadRole", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    if (readGroup != null)
                        policy.RequireRole(readGroup);
                });

            });

            services.AddAuthentication(IISDefaults.AuthenticationScheme);

            services.Configure<IISOptions>(options =>
            {
                options.AutomaticAuthentication = true;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
              //  app.UseHsts();
            }
            
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials());
            app.UseAuthentication();
            app.UseDefaultFiles();
            app.UseStaticFiles();
           // app.UseHttpsRedirection();
            app.UseMvc();

            app.Run(async (context) =>
            {
                context.Response.ContentType = "text/html";
                await context.Response.SendFileAsync(Path.Combine(env.WebRootPath, "index.html"));
            });
        }
    }
}
