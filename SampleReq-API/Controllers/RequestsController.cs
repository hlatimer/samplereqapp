﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SampleReq_API.Data;
using SampleReq_API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Data.SqlClient;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace SampleReq_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RequestsController : ControllerBase
    {
        private readonly DataContext _context;

        public RequestsController (DataContext context) 
        {
            _context = context;
        }
        // GET api/requests
        [HttpGet]
        public async Task<IActionResult> GetRequests()
        { 
            var values = await _context.NewRequest.ToListAsync();
            return Ok(values);
        }

        [HttpGet("prodnames")]
        public async Task<IActionResult> GetProdNames()
        {
                    SqlConnection cnn = new SqlConnection(@"Server=FC-SQL-V\DEV;Database=FSky_Apps;Trusted_Connection=True;");
					SqlCommand cmd = new SqlCommand();
		            string parm1 = "%";

					//Develope SQL string to be executed
		            cmd.Connection = cnn;
		            cmd.CommandType = System.Data.CommandType.StoredProcedure;
		            cmd.CommandText = "fc_sp_prodnames";
		            cmd.Parameters.Add(new SqlParameter("@parm1",parm1));
		            cnn.Open();

					//declare reader object to iterate through sp record set
                    var scalar = new StringBuilder();
		            SqlDataReader reader = cmd.ExecuteReader();
                    int column = reader.GetOrdinal("productname");

                    List<SelectListItem> LstFile = new List<SelectListItem>();
					//populate list with bin id's
		            if (reader.HasRows)
		            {
			           while (reader.Read())
			            {
				            LstFile.Add(new SelectListItem()
				            {
					            Text = reader.GetValue(column).ToString()
				            }
				            );
			            }
		            }
					reader.Close();
		            cnn.Close();

                    var jObject = JsonConvert.SerializeObject(LstFile,Formatting.Indented);
                    
            
            return Ok(jObject);
        }

         [HttpGet("prodnamesreq")]
        public async Task<IActionResult> GetProdNamesReq()
        {
                    SqlConnection cnn = new SqlConnection(@"Server=FC-SQL-V\DEV;Database=FSky_Apps;Trusted_Connection=True;");
					SqlCommand cmd = new SqlCommand();
		            string parm1 = "%";

					//Develope SQL string to be executed
		            cmd.Connection = cnn;
		            cmd.CommandType = System.Data.CommandType.StoredProcedure;
		            cmd.CommandText = "fc_sp_prodnames_reqapp";
		            cmd.Parameters.Add(new SqlParameter("@parm1",parm1));
		            cnn.Open();

					//declare reader object to iterate through sp record set
                    var scalar = new StringBuilder();
		            SqlDataReader reader = cmd.ExecuteReader();
                    int column = reader.GetOrdinal("product");

                    List<SelectListItem> LstFile = new List<SelectListItem>();
					//populate list with bin id's
		            if (reader.HasRows)
		            {
			           while (reader.Read())
			            {
				            LstFile.Add(new SelectListItem()
				            {
					            Text = reader.GetValue(column).ToString()
				            }
				            );
			            }
		            }
					reader.Close();
		            cnn.Close();

                    var jObject = JsonConvert.SerializeObject(LstFile,Formatting.Indented);
                    
            
            return Ok(jObject);
        }


        // POST api/requests
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] NewRequest newrequest)
        {
            // Connect to Database and initialize SqlCommand object to run sp
            SqlConnection cnn = new SqlConnection(@"Server=FC-SQL-V\DEV;Database=FSky_Apps;Trusted_Connection=True;");
			SqlCommand cmd = new SqlCommand();

            // Convert NewRequest data Model and convert to JSON object
            var jObject = Newtonsoft.Json.JsonConvert.SerializeObject(newrequest);

            // Parameters to send to sp
		    var parm1 = jObject;
            int parm2 = 1;


            if (ModelState.IsValid)
            {
                // Save request
                _context.Update(newrequest);
                await _context.SaveChangesAsync();

                // Run sp to e-mail Power user about new request
                cmd.Connection = cnn;
		        cmd.CommandType = System.Data.CommandType.StoredProcedure;
		        cmd.CommandText = "fc_sp_Email_SampleReqs";
		        cmd.Parameters.Add(new SqlParameter("@input",parm1));
                cmd.Parameters.Add(new SqlParameter("@parm1",parm2));
		        cnn.Open();
                cmd.ExecuteNonQuery();
                cnn.Close();

                return StatusCode(201);
            }

            return BadRequest("this does not work");
        }

         // POST api/requests
        [HttpPost("editsalert")]
        public async Task<IActionResult> EditsAlertAsync([FromBody] NewRequest newrequest)
        {
            // Connect to Database and initialize SqlCommand object to run sp
            SqlConnection cnn = new SqlConnection(@"Server=FC-SQL-V\DEV;Database=FSky_Apps;Trusted_Connection=True;");
			SqlCommand cmd = new SqlCommand();

            // Convert NewRequest data Model and convert to JSON object
            var jObject = Newtonsoft.Json.JsonConvert.SerializeObject(newrequest);

            // Parameters to send to sp
            var parm1 = jObject;
            int parm2 = 2;

            if (ModelState.IsValid)
            {
                // sp to alert specific user associated with submitted request to any 
                // changes to the request
                cmd.Connection = cnn;
		        cmd.CommandType = System.Data.CommandType.StoredProcedure;
		        cmd.CommandText = "fc_sp_Email_SampleReqs";
		        cmd.Parameters.Add(new SqlParameter("@input",parm1));
                cmd.Parameters.Add(new SqlParameter("@parm1",parm2));
		        cnn.Open();
                cmd.ExecuteNonQuery();
                cnn.Close();

                return StatusCode(201);
            }

            return BadRequest("this does not work");
        }

        // POST api/requests/edits
        [HttpPost("edits")]
        public async Task<IActionResult> Edits([FromBody] NewRequest [] newrequest)
        {
            // Save edits to requests using EF 
            if (ModelState.IsValid)
            {
                int i = 0;
                foreach (var obj in newrequest){

                    _context.Update(newrequest[i]);
                    i++;
                }
                await _context.SaveChangesAsync();
                return StatusCode(201);
            }

            return BadRequest("this does not work");
        }

         [HttpPost("newprod")]
        public async Task<IActionResult> PostProd([FromBody] newProd newProd)
        {
            // Connect to Database and initialize SqlCommand object to run sp
            SqlConnection cnn = new SqlConnection(@"Server=FC-SQL-V\DEV;Database=FSky_Apps;Trusted_Connection=True;");
			 SqlCommand cmd = new SqlCommand();

            // Parameters to send to sp
            string parm1 = newProd.productname;
             string parm2 = newProd.user;

			 //Develope SQL string to be executed
            // save new product to be selectable in sample request form 
		     cmd.Connection = cnn;
		     cmd.CommandType = System.Data.CommandType.StoredProcedure;
		     cmd.CommandText = "fc_sp_Add_ProdNames";
		     cmd.Parameters.Add(new SqlParameter("@parm1",parm1));
             cmd.Parameters.Add(new SqlParameter("@parm2",parm2));
		     cnn.Open();
             int isValid = cmd.ExecuteNonQuery();
             cnn.Close();

            if (isValid == 1){
                return StatusCode(201);
            }
            else 
                return BadRequest();
        
            
        }

        
    }
}
