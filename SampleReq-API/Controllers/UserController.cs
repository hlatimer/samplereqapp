using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Principal;
using Microsoft.AspNetCore.Cors;
using SampleReq_API.Helpers;

namespace SampleReq_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        [Authorize(Policy = "RequireAdminRole")]
        [HttpGet]
        public IActionResult GetUser()
        {
            var userId = User.Identity.Name;
            // var userId = "FC\\HLATIMER";
            var userGroups = GetGroups(User.Identity.ToString());
            var secGroup = GetSecurityGroup(userGroups);
            var user = new
            {
                UserId = userId,
                SecurityGroup = secGroup
            };

            return Ok(user);
        }

        private string GetSecurityGroup(List<string> groups)
        {
            var adminGroup = AppConfigurationSettings.AppSetting["WindowsAdminGroup"];
            var editGroup = AppConfigurationSettings.AppSetting["WindowsEditGroup"];
            var readGroup = AppConfigurationSettings.AppSetting["WindowsReadGroup"];

            var secGroup = groups.FirstOrDefault(s => s.Contains(adminGroup));

            var secLevel = "";

            if (secGroup != null)
            {
                secLevel = "Admin";
            }
            else
            {
                secGroup = groups.FirstOrDefault(s => s.Contains(editGroup));

                if (secGroup != null)
                {
                    secLevel = "Edit";
                }
                else
                {
                    secGroup = groups.FirstOrDefault(s => s.Contains(readGroup));

                    if (secGroup != null)
                    {
                        secLevel = "Read";
                    }
                    else
                    {
                        secGroup = "none";
                    }
                }
            }

            return secLevel;

        }

        private List<string> GetGroups(string userName)
        {
            List<string> result = new List<string>();
            WindowsIdentity wi = WindowsIdentity.GetCurrent();

            foreach (IdentityReference group in wi.Groups)
            {
                try
                {
                    result.Add(group.Translate(typeof(NTAccount)).ToString());
                }
                catch (Exception ex) { }
            }

            result.Sort();
            return result;
        }

    }
}